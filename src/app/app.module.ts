import { FlexLayoutModule } from "@angular/flex-layout";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { TreechartComponent } from './treechart/treechart.component';

@NgModule({
  imports: [
    BrowserModule,
    FlexLayoutModule,
  ],
  declarations: [AppComponent, TreechartComponent],
  bootstrap: [ AppComponent ]
  // ...
})
export class AppModule {}